import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/screen/home/home_navigator.dart';
import 'package:palala_app/ui/widget/button/custom_button_primary.dart';
import 'package:palala_app/ui/widget/button/custom_button_social_media.dart';
import 'package:palala_app/ui/widget/text_field/custom_text_form.dart';

class RegisterPage extends StatefulWidget {
  final String email;
  RegisterPage(this.email);
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController tec = new TextEditingController();
  TextEditingController tec1 = new TextEditingController();
  TextEditingController tec2 = new TextEditingController();
  TextEditingController tec3 = new TextEditingController();
  TextEditingController tec4 = new TextEditingController();
  FocusNode myFocusNode = new FocusNode();
  FocusNode myFocusNode1 = new FocusNode();
  FocusNode myFocusNode2 = new FocusNode();
  FocusNode myFocusNode3 = new FocusNode();
  FocusNode myFocusNode4 = new FocusNode();
  bool enableButton = false, isButtonLoading = false,showPassword = true;


  @override
  void initState() {
    // TODO: implement initState
    tec1.text = widget.email;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 24),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 14,
                  width: 14,
                  margin: EdgeInsets.symmetric(vertical: 45),
                  child: InkWell(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Icon(Icons.arrow_back_rounded)),
                ),
                Container(
                  child: Text("Informasi Tambahan",style: Fonts.h1(),),
                ),
                SizedBox(height: 32,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: CustomTextForm(
                    "Nama pengguna",
                    "Masukkan nama Pengguna",
                    textEditingController: tec,
                    type: 1,
                    focusNode: myFocusNode,
                    borderRadius: 6,
                    onTap: (){
                      FocusScope.of(context).unfocus();
                      Future.delayed(Duration(milliseconds: 300),(){
                        myFocusNode.requestFocus();
                      });
                    },
                    onChanged: (tes){
                      if(tes.isNotEmpty && tec1.text.isNotEmpty && tec2.text.isNotEmpty && tec3.text.isNotEmpty && tec4.text.isNotEmpty){
                        setState(() {
                          enableButton = true;
                        });
                      } else {
                        setState(() {
                          enableButton = false;
                        });
                      }
                    },
                  ),
                ),
                SizedBox(height: 24,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: CustomTextForm(
                    "Email",
                    "Masukkan email",
                    readOnly: true,
                    textEditingController: tec1,
                    type: 2,
                    focusNode: myFocusNode1,
                    borderRadius: 6,
                    // onTap: (){
                    //   FocusScope.of(context).unfocus();
                    //   Future.delayed(Duration(milliseconds: 50),(){
                    //     myFocusNode1.requestFocus();
                    //   });
                    // },
                    onChanged: (tes){
                      if(tes.isNotEmpty){
                        setState(() {
                          enableButton = true;
                        });
                      } else {
                        setState(() {
                          enableButton = false;
                        });
                      }
                    },
                  ),
                ),
                SizedBox(height: 24,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: CustomTextForm(
                    "Nama Depan",
                    "Masukkan nama depan",
                    textEditingController: tec2,
                    type: 1,
                    focusNode: myFocusNode2,
                    borderRadius: 6,
                    onTap: (){
                      FocusScope.of(context).unfocus();
                      Future.delayed(Duration(milliseconds: 300),(){
                        myFocusNode2.requestFocus();
                      });
                    },
                    onChanged: (tes){
                      if(tes.isNotEmpty && tec1.text.isNotEmpty && tec.text.isNotEmpty && tec3.text.isNotEmpty && tec4.text.isNotEmpty){
                        setState(() {
                          enableButton = true;
                        });
                      } else {
                        setState(() {
                          enableButton = false;
                        });
                      }
                    },
                  ),
                ),
                SizedBox(height: 24,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: CustomTextForm(
                    "Nama belakang",
                    "Masukkan nama belakang",
                    textEditingController: tec3,
                    type: 1,
                    focusNode: myFocusNode3,
                    borderRadius: 6,
                    onTap: (){
                      FocusScope.of(context).unfocus();
                      Future.delayed(Duration(milliseconds: 300),(){
                        myFocusNode3.requestFocus();
                      });
                    },
                    onChanged: (tes){
                      if(tes.isNotEmpty && tec1.text.isNotEmpty && tec.text.isNotEmpty && tec2.text.isNotEmpty && tec4.text.isNotEmpty){
                        setState(() {
                          enableButton = true;
                        });
                      } else {
                        setState(() {
                          enableButton = false;
                        });
                      }
                    },
                  ),
                ),
                SizedBox(height: 24,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: CustomTextForm(
                    "Kata sandi",
                    "Masukkan kata sandi",
                    textEditingController: tec4,
                    type: 3,
                    showPassword: showPassword,
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          showPassword = !showPassword;
                        });
                      },
                      child: Icon(showPassword
                          ? Icons.visibility_off
                          : Icons.visibility, size: 16,color: color_secondary),
                    ),
                    focusNode: myFocusNode4,
                    borderRadius: 6,
                    onTap: (){
                      FocusScope.of(context).unfocus();
                      Future.delayed(Duration(milliseconds: 300),(){
                        myFocusNode4.requestFocus();
                      });
                    },
                    onChanged: (tes){
                      if(tes.isNotEmpty && tec1.text.isNotEmpty && tec.text.isNotEmpty && tec3.text.isNotEmpty && tec2.text.isNotEmpty){
                        setState(() {
                          enableButton = true;
                        });
                      } else {
                        setState(() {
                          enableButton = false;
                        });
                      }
                    },
                  ),
                ),
                SizedBox(height: 32,),
                Container(
                  child: RichText(
                    text: TextSpan(
                        text: 'Dengan klik lanjut, berarti saya menyetujui',
                        style: Fonts.st(size: 12),
                        children: <TextSpan>[
                          TextSpan(text: ' Ketentuan layanan',
                              style: Fonts.st(size: 12).merge(TextStyle(decoration: TextDecoration.underline,)),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  print("ketentuan layanan");
                                  myFocusNode4.requestFocus();
                                }
                          ),
                          TextSpan(text: ' dan',
                              style: Fonts.st(size: 12),
                          ),
                          TextSpan(text: ' Kebijakan privasi',
                              style: Fonts.st(size: 12).merge(TextStyle(decoration: TextDecoration.underline,)),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  myFocusNode1.requestFocus();
                                  print(" Kebikajan privasi");
                                }
                          ),
                          TextSpan(text: ' yang berlaku',
                              style: Fonts.st(size: 12),
                          )
                        ]
                    ),
                  ),
                ),
                SizedBox(height: 17,),
                Container(
                  height: 48,
                  width: MediaQuery.of(context).size.width,
                  child: CustomButtonPrimary(
                    color: color_secondary,
                    title: "Lanjut",
                    textColor: Colors.white,
                    isEnable: enableButton,
                    btnLoading:  isButtonLoading,
                    onTap: (){
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeNavigation()));
                    },
                    borderRadius: 6,
                  ),
                ),
                SizedBox(height: 16,),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
