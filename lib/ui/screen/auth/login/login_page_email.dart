import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palala_app/constant/assets_resource.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/screen/auth/register/register_page.dart';
import 'package:palala_app/ui/widget/button/custom_button_primary.dart';
import 'package:palala_app/ui/widget/button/custom_button_social_media.dart';
import 'package:palala_app/ui/widget/text_field/custom_text_form.dart';

class LoginPageEmail extends StatefulWidget {
  @override
  _LoginPageEmailState createState() => _LoginPageEmailState();
}

class _LoginPageEmailState extends State<LoginPageEmail> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  FocusNode myFocusNode = new FocusNode();
  TextEditingController teEmail = new TextEditingController();
  bool enableButton = false, isButtonLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    checkEmail();
    super.initState();
  }

  void checkEmail(){
    var email = "fredrik@gmail";

    print("validate => "+EmailValidator.validate(email).toString());
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 24),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 14,
                  width: 14,
                  margin: EdgeInsets.symmetric(vertical: 45),
                  child: Icon(Icons.close_rounded),
                ),
                Container(
                  child: Text("Masuk atau daftar",style: Fonts.h1(),),
                ),
                SizedBox(height: 32,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: CustomTextForm(
                    "email",
                    "Masukkan Email",
                    textEditingController: teEmail,
                    type: 2,
                    focusNode: myFocusNode,
                    borderRadius: 6,
                    onChanged: (tes){
                      if(tes.isNotEmpty){
                        setState(() {
                          enableButton = true;
                        });
                      } else {
                        setState(() {
                          enableButton = false;
                        });
                      }
                    },
                  ),
                ),
                SizedBox(height: 16,),
                Container(
                  height: 48,
                  width: MediaQuery.of(context).size.width,
                  child: CustomButtonPrimary(
                    color: color_secondary,
                    title: "Lanjut",
                    textColor: Colors.white,
                    isEnable: enableButton,
                    btnLoading:  isButtonLoading,
                    onTap: (){

                      Future.delayed(Duration(seconds: 1),(){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterPage(teEmail.text)));
                      });
                    },
                    borderRadius: 6,
                  ),
                ),
                SizedBox(height: 16,),
                Container(
                  child: Stack(
                    children: [
                      Divider(),
                      Center(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 7),
                            decoration: BoxDecoration(
                              color: Colors.white
                            ),
                            child: Text("atau",style: Fonts.st(size: 12,),)),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 16,),
                Container(
                  height: 48,
                  // width: MediaQuery.of(context).size.width/2,
                  child: CustomButtonSocialMedia(
                    color: color_secondary,
                    title: "Google",
                    textColor:color_secondary,
                    isEnable: true,
                    icon: Image.asset(AssetsResources.icGoogle),
                    spaceIcon: 10,
                    btnLoading:  false,
                    onTap: (){
                      print("google coy");
                    },
                    borderRadius: 6,
                  ),
                ),
                SizedBox(height: 16,),
                Container(
                  height: 48,
                  // width: MediaQuery.of(context).size.width/2,
                  child: CustomButtonSocialMedia(
                    color: color_secondary,
                    title: "Facebook",
                    textColor:color_secondary,
                    isEnable: true,
                    icon: Image.asset(AssetsResources.icFacebook),
                    spaceIcon: 10,
                    btnLoading:  false,
                    onTap: (){
                      print("pesbuk coy");

                    },
                    borderRadius: 6,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
