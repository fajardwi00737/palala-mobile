import 'package:flutter/material.dart';
import 'package:palala_app/constant/assets_resource.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/widget/button/custom_button_primary.dart';
import 'package:palala_app/ui/widget/button/custom_button_social_media.dart';
import 'package:palala_app/ui/widget/text_field/custom_text_form.dart';

class LoginPagePassword extends StatefulWidget {
  @override
  _LoginPagePasswordState createState() => _LoginPagePasswordState();
}

class _LoginPagePasswordState extends State<LoginPagePassword> {
  FocusNode myFocusNode = new FocusNode();
  bool enableButton = false, isButtonLoading = false,showPassword  = true;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: 24),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 14,
                  width: 14,
                  margin: EdgeInsets.symmetric(vertical: 45),
                  child: Icon(Icons.close_rounded),
                ),
                Container(
                  child: Text("Selamat datang Nika!",style: Fonts.h1(),),
                ),
                SizedBox(height: 16,),
                Container(
                  child: Text("Silahkan masukkan kata sandi untuk melanjutkan.",style: Fonts.st1(),),
                ),
                SizedBox(height: 24,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: CustomTextForm(
                    "Kata sandi",
                    "Masukkan kata sandi",
                    type: 3,
                    showPassword: showPassword,
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          showPassword = !showPassword;
                        });
                      },
                      child: Icon(showPassword
                          ? Icons.visibility_off
                          : Icons.visibility, size: 16,color: color_secondary),
                    ),
                    focusNode: myFocusNode,
                    borderRadius: 6,
                    onChanged: (tes){
                      if(tes.isNotEmpty){
                        setState(() {
                          enableButton = true;
                        });
                      } else {
                        setState(() {
                          enableButton = false;
                        });
                      }
                    },
                  ),
                ),
                SizedBox(height: 32,),
                Container(
                  height: 48,
                  width: MediaQuery.of(context).size.width,
                  child: CustomButtonPrimary(
                    color: color_secondary,
                    title: "Lanjut",
                    textColor: Colors.white,
                    isEnable: enableButton,
                    btnLoading:  isButtonLoading,
                    onTap: (){

                    },
                    borderRadius: 6,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
