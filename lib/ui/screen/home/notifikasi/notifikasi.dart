import 'package:flutter/material.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/models/inbox_model/inbox_model.dart';
import 'package:palala_app/ui/widget/card/inbox_card.dart';

class NotifikasiPage extends StatefulWidget {
  @override
  _NotifikasiPageState createState() => _NotifikasiPageState();
}

class _NotifikasiPageState extends State<NotifikasiPage> {

  final List<InboxCardModel> cardContent = [
    InboxCardModel("Fajar","Terima kasih banyak yaa","5 des 2022","10:10"),
    InboxCardModel("Celine","Acara nya seruuu","2 Feb 2022","12:31"),
    InboxCardModel("Haidar","Bulan depan lagi ya","6 Jun 2022","21:16"),
    InboxCardModel("Messi","Mantap bang","5 des 2022","09:12"),
    // CardContent(Icons.article_outlined, "Daftar"),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.all(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Inbox",style: Fonts.sh1(),),
              SizedBox(height: 22,),
              Divider(thickness: 1,),
              SizedBox(height: 22,),
              Text("${cardContent.length} pesan belum terbaca",style: Fonts.st2(),),
              SizedBox(height: 23,),
              Expanded(
                child: ListView.builder(
                  itemCount: cardContent.length,
                    itemBuilder: (context,index){
                      return InboxCard(cardContent[index].name, cardContent[index].message, cardContent[index].time, cardContent[index].date);
                    }
                ),
              ),
              // InboxCard("Fajar", "Terima kasih banyak ya", "7 sep 2022", "12.30")
            ],
          ),
        ),
      ),
    );
  }
}
