import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/widget/button/custom_border_button.dart';
import 'package:palala_app/ui/widget/button/custom_button_primary.dart';
import 'package:palala_app/ui/widget/card/main_cart_card.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_rounded,size: 25,color: color_secondary,),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        title: Text("Wisata Tur Majalengka Jawa Barat",style: Fonts.sh3(),),
        centerTitle: true,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Container(
        height: 150,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(top: BorderSide(color: color_secondary.withOpacity(0.12)))
        ),
        padding: EdgeInsets.symmetric(horizontal: 24,vertical: 16),
        width: MediaQuery.of(context).size.width,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: 1,
              child: Container(
                child: RichText(
                  text: TextSpan(
                      text: 'Dengan klik lanjut, berarti saya menyetujui',
                      style: Fonts.st(size: 12),
                      children: <TextSpan>[
                        TextSpan(text: ' Ketentuan layanan',
                            style: Fonts.st(size: 12).merge(TextStyle(decoration: TextDecoration.underline,)),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                print("ketentuan layanan");
                                // myFocusNode4.requestFocus();
                              }
                        ),
                        TextSpan(text: ' dan',
                          style: Fonts.st(size: 12),
                        ),
                        TextSpan(text: ' Kebijakan privasi',
                            style: Fonts.st(size: 12).merge(TextStyle(decoration: TextDecoration.underline,)),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                // myFocusNode1.requestFocus();
                                print(" Kebikajan privasi");
                              }
                        ),
                        TextSpan(text: ' yang berlaku',
                          style: Fonts.st(size: 12),
                        )
                      ]
                  ),
                ),
              ),
            ),
            SizedBox(height: 24,),
            Flexible(
              flex: 1,
              child: Container(
                child: CustomButtonPrimary(
                  color: color_primary,
                  title: "Bayar sekarang",
                  textColor: Colors.white,
                  isEnable: true,
                  btnLoading:  false,
                  borderRadius: 6,
                  onTap: (){
                    // Navigator.push(context, MaterialPageRoute(builder: (context)=>CartPage()));
                  },

                ),
              ),
            )
          ],
        ),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: EdgeInsets.all(24),
                  child: MainCartCard()),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  color: color_secondary.withOpacity(0.12),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 24,horizontal: 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Tanggal dipilih",style: Fonts.sh3(),),
                    SizedBox(
                      height: 8,
                    ),
                    Text("Sabtu, 17-24 September 2022",style: Fonts.st1(color: color_secondary.withOpacity(0.38)),)
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  color: color_secondary.withOpacity(0.12),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 5,
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Dewasa",style: Fonts.st1(color: color_secondary)),
                            SizedBox(
                              height: 4,
                            ),
                            Text("13 tahun ke atas",style: Fonts.st2(color: color_secondary.withOpacity(0.38)))
                          ],
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          height: 32,
                          width: 32,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: color_secondary)
                          ),
                          child: Center(child: Icon(Icons.remove_rounded)),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text("1"),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          height: 32,
                          width: 32,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(color: color_secondary)
                          ),
                          child: Center(child: Icon(Icons.add_rounded)),
                        ),
                        SizedBox(
                          width: 24,
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Container(
                child: Divider(
                  thickness: 8,
                  color: color_secondary.withOpacity(0.05),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 5,
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Informasi peserta",style: Fonts.sh3(color: color_secondary)),
                            SizedBox(
                              height: 16,
                            ),
                            Text("Fajar jos",style: Fonts.st1(color: color_secondary)),
                            SizedBox(
                              height: 4,
                            ),
                            Text("fajar@mail.com",style: Fonts.st2(color: color_secondary.withOpacity(0.38)))
                          ],
                        ),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(right: 24),
                        child: Text("Ubah",style: Fonts.sh(size: 14,color: color_secondary).merge(TextStyle(decoration: TextDecoration.underline)))),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  thickness: 8,
                  color: color_secondary.withOpacity(0.05),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Detail harga",style: Fonts.sh3(color: color_secondary))),
              Container(
                margin: EdgeInsets.symmetric(vertical: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 5,
                      child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 24),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("1 dewasa x Rp200.000",style: Fonts.st1(color: color_secondary.withOpacity(0.38))),
                            SizedBox(
                              height: 4,
                            ),
                            Text("Total",style: Fonts.sh3(color: color_secondary))
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 24),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Rp200.000",style: Fonts.st1(color: color_secondary.withOpacity(0.38))),
                          SizedBox(
                            height: 4,
                          ),
                          Text("Rp200.000",style: Fonts.sh3(color: color_secondary))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  thickness: 8,
                  color: color_secondary.withOpacity(0.05),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Detail harga",style: Fonts.sh3(color: color_secondary))),
              SizedBox(
                height: 16,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),

                  height: 48,
                  width: MediaQuery.of(context).size.width,
                  child: CustomBorderButton(
                    isEnable: true,
                    onTap: (){
                      // Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailPaket()));
                    },
                    title: "Pilih metode pembayaran",borderRadius: 6,)),
              SizedBox(
                height: 32,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Pakai promo",style: Fonts.sh3(color: color_secondary).merge(TextStyle(decoration: TextDecoration.underline)))),
              SizedBox(
                height: 16,
              ),
              Container(
                child: Divider(
                  thickness: 8,
                  color: color_secondary.withOpacity(0.05),
                ),
              ),
              SizedBox(
                height: 147,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
