import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/widget/card/paket_card.dart';
import 'package:palala_app/ui/widget/card/paket_detail_card.dart';
import 'package:palala_app/ui/widget/card/review_detail_card.dart';

class DetailReview extends StatefulWidget {
  @override
  _DetailReviewState createState() => _DetailReviewState();
}

class _DetailReviewState extends State<DetailReview> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: Colors.white,
          padding: EdgeInsets.all(24),

          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                IconButton(
                  onPressed: (){
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back_rounded,size: 25,color: color_secondary.withOpacity(0.87),),
                ),
                SizedBox(height: 45,),
                Text("13 Penilaian (5.0)",style: Fonts.sh1(),),
                SizedBox(height: 24,),
                ReviewDetailCard(),
                SizedBox(height: 32,),
                ReviewDetailCard(),
                SizedBox(height: 32,),
                ReviewDetailCard(),
                SizedBox(height: 32,),
                ReviewDetailCard(),
                SizedBox(height: 32,),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
