import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/screen/home/beranda/cart_page.dart';
import 'package:palala_app/ui/screen/home/beranda/detail_paket.dart';
import 'package:palala_app/ui/screen/home/beranda/detail_review.dart';
import 'package:palala_app/ui/widget/bottomsheet/addons_bottomsheet.dart';
import 'package:palala_app/ui/widget/bottomsheet/contact_host.dart';
import 'package:palala_app/ui/widget/button/custom_border_button.dart';
import 'package:palala_app/ui/widget/button/custom_button_primary.dart';
import 'package:palala_app/ui/widget/card/addons_card.dart';
import 'package:palala_app/ui/widget/card/experience_organizer_card.dart';
import 'package:palala_app/ui/widget/card/paket_card.dart';
import 'package:palala_app/ui/widget/card/review_card.dart';
import 'package:palala_app/ui/widget/read_more_text/read_more_text.dart';

class DetailTripPage extends StatefulWidget {
  @override
  _DetailTripPageState createState() => _DetailTripPageState();
}

class _DetailTripPageState extends State<DetailTripPage> {
  int _currentSlideIndex = 0;
  List<Color> colorList = [
    Colors.green.shade100,
    Colors.green.shade200,
    Colors.green.shade300,
    Colors.green.shade400,
    Colors.green.shade500,
    Colors.green.shade600,
  ];

  showAddonsBottomsheet(){
    showModalBottomSheet(
        context: context,
        // shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(16),topLeft:  Radius.circular(16))),
        builder: (context){
          return AddonsBottomsheet();
        }
    );
  }

  showContactHostBottomsheet(){
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        // shape: RoundedRectangleBorder(
        //     borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        // shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(16),topLeft:  Radius.circular(16))),
        builder: (context){
          return ContactHost();
        }
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Container(
        height: 80,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(top: BorderSide(color: color_secondary.withOpacity(0.12)))
        ),
        padding: EdgeInsets.symmetric(horizontal: 24,vertical: 16),
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Sab, 17 Sep",style: Fonts.st2(color: color_secondary.withOpacity(0.5)),),
                  Text("Rp200.000 /org",style: Fonts.sh3(color: color_amber),)
                ],
              ),
            ),
            SizedBox(width: 16,),
            Flexible(
              flex: 1,
              child: Container(
                child: CustomButtonPrimary(
                  color: color_primary,
                  title: "Pesan sekarang",
                  textColor: Colors.white,
                  isEnable: true,
                  btnLoading:  false,
                  borderRadius: 6,
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>CartPage()));
                  },

                ),
              ),
            )
          ],
        ),
      ),
      body: Container(
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  CarouselSlider.builder(
                     itemCount: colorList.length,
                    itemBuilder: (context, idx, index) {
                       return Container(
                         height: 480,
                         width: MediaQuery.of(context).size.width,
                         color: colorList[idx],
                       );
                    },
                    options: CarouselOptions(
                        height: 480,
                        autoPlay: false,
                        aspectRatio: 2.0,
                        enlargeCenterPage: false,
                        disableCenter: true,
                        // enlargeStrategy: CenterPageEnlargeStrategy.scale,
                        viewportFraction: 1.0,
                        // enlargeCenterPage: false,
                        // autoPlay: true,
                        autoPlayInterval: Duration(seconds: 3),
                        onPageChanged: (index, reason) {
                          setState(() {
                            _currentSlideIndex = index;
                          });
                        }),
                       ),
                  Positioned(
                    top: 0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: MediaQuery.of(context).viewPadding.top + 8),
                        padding: EdgeInsets.symmetric(horizontal: 24),
                        child: Row(
                          children: [
                            Container(
                              width: 45,
                              height: 45,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle
                              ),
                              child: Center(
                                child: Icon(Icons.arrow_back_rounded),
                              ),
                            ),
                            Spacer(),
                            Container(
                              width: 45,
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle
                              ),
                              child: Center(
                                child: Icon(Icons.ios_share),
                              ),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Container(
                              width: 45,
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle
                              ),
                              child: Center(
                                child: Icon(Icons.favorite_border_rounded),
                              ),
                            )
                          ],
                        ),
                      )
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: this.colorList.map((value) {
                          int index = colorList.indexOf(value);
                          return Container(
                            width: 8.0,
                            height: 8.0,
                            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: _currentSlideIndex == index
                                  ? Color(0xFFE52920)
                                  : Color(0xFFDADADA),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Wisata Tur Majalengka Jawa Barat",maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.sh1(),)),
              SizedBox(
                height: 8,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  children: [
                    Icon(Icons.star_rounded),
                    SizedBox(
                      width: 4,
                    ),
                    Text("4,72",style: Fonts.st3(),),
                    SizedBox(
                      width: 4,
                    ),
                    Text("(8 Review)",style: Fonts.st3(),),
                    SizedBox(
                      width: 4,
                    ),
                    Text("• Banten",style: Fonts.st3(),),
                  ],
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  color: color_secondary.withOpacity(0.12),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Deskripsi",maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.sh1(),)),
              SizedBox(
                height: 8,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: ReadMoreText(
                  'Flutter has its own UI components, along with an engine to render them on both the Android and iOS platforms. Most of those UI components, right out of the box, conform to the guidelines of Material Design.',
                  trimLines: 3,
                  colorClickableText: Colors.pink,
                  trimMode: TrimMode.Line,
                  trimCollapsedText: '\nBaca lebih lanjut',
                  trimExpandedText: 'Tutup',
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  color: color_secondary.withOpacity(0.12),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Sudah termasuk",maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.sh1(),)),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                height: 235,
                child: ListView(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  children: [
                    AddonsCard(
                      onTap: (){
                        showAddonsBottomsheet();
                      },
                    ),
                    SizedBox(width: 12,),
                    AddonsCard(
                      onTap: (){
                        showAddonsBottomsheet();
                      },
                    ),
                    SizedBox(width: 12,),
                    AddonsCard(
                      onTap: (){
                        showAddonsBottomsheet();
                      },
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  color: color_secondary.withOpacity(0.12),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Detail lokasi",maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.sh1(),)),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                width: MediaQuery.of(context).size.width,
                height: 208,
                decoration: BoxDecoration(
                  color: color_grey,
                  borderRadius: BorderRadius.circular(12)
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Titik penjemputan",maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.sh(size: 14),)),
              SizedBox(
                height: 4,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Harap siap untuk penjemputan pada waktu yang telah dikonfirmasi di hotel Anda di Ubud, Canggu, Seminyak, Legian, Kuta, Tuban, Jimbaran, Sanur, Tanjung Benoa, atau Nusa Dua Tiga Empat.",style: Fonts.st2(),)),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  color: color_secondary.withOpacity(0.12),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Pilihan paket",maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.sh1(),)),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                height: 145,
                child: ListView(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  children: [
                    PaketCard("Sab, 17 Sep", "2:00 PM - 4:00 PM", "Sudah 100 dipesan", "Rp200.000 /org"),
                    SizedBox(width: 12,),
                    PaketCard("Sab, 17 Sep", "2:00 PM - 4:00 PM", "Sudah 100 dipesan", "Rp200.000 /org"),
                    SizedBox(width: 12,),
                    PaketCard("Sab, 17 Sep", "2:00 PM - 4:00 PM", "Sudah 100 dipesan", "Rp200.000 /org")
                  ],
                ),
              ),
              SizedBox(
                height: 24,
              ),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 24),

                height: 48,
                  width: MediaQuery.of(context).size.width,
                  child: CustomBorderButton(
                    isEnable: true,
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailPaket()));
                    },
                    title: "Lihat Semua Paket",borderRadius: 6,)),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  color: color_secondary.withOpacity(0.12),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: ExperienceOrganizerCard()),
              SizedBox(
                height: 24,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),

                  height: 48,
                  width: MediaQuery.of(context).size.width,
                  child: CustomBorderButton(
                    isEnable: true,
                    onTap: (){
                      showContactHostBottomsheet();
                    },
                    title: "Kontak Fajar",borderRadius: 6,)),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  color: color_secondary.withOpacity(0.12),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("13 Penilaian (5.0)",maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.sh1(),)),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                height: 193,
                child: ListView(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  children: [
                    ReviewCard(),
                    SizedBox(width: 12,),
                    ReviewCard(),
                    SizedBox(width: 12,),
                    ReviewCard()
                  ],
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),

                  height: 48,
                  width: MediaQuery.of(context).size.width,
                  child: CustomBorderButton(
                    isEnable: true,
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailReview()));
                    },
                    title: "Lihat semua penilaian",borderRadius: 6,)),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  color: color_secondary.withOpacity(0.12),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  child: Text("Informasi tambahan",maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.sh1(),)),
              SizedBox(
                height: 24,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Flexible(
                            flex:1,
                            child: Container(
                              height: 40,
                              width: 40,

                              child: Icon(Icons.local_movies_rounded),
                            ),
                          ),
                          SizedBox(width: 16,),
                          Expanded(
                              flex:10,
                              child: Text("Penukaran tiket",maxLines: 1,overflow: TextOverflow.ellipsis,style: Fonts.sh(size: 14),)),
                          Flexible(
                            flex:1,
                            // height: 40,
                            // width: 40,
                            child: Icon(Icons.chevron_right_rounded),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Divider(
                        color: color_secondary.withOpacity(0.12),
                      ),
                    ),
                    Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Flexible(
                            flex:1,
                            child: Container(
                              height: 40,
                              width: 40,

                              child: Icon(Icons.info_rounded),
                            ),
                          ),
                          SizedBox(width: 16,),
                          Expanded(
                              flex:10,
                              child: Text("Syarat dan ketentuan",maxLines: 1,overflow: TextOverflow.ellipsis,style: Fonts.sh(size: 14),)),
                          Flexible(
                            flex:1,
                            // height: 40,
                            // width: 40,
                            child: Icon(Icons.chevron_right_rounded),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 112,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
