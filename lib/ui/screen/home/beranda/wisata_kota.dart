import 'package:flutter/material.dart';
import 'package:like_button/like_button.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/models/card_model/experience_card_model.dart';
import 'package:palala_app/ui/screen/home/beranda/detail_trip_page.dart';
import 'package:palala_app/ui/widget/card/experience_card.dart';

class WisataKota extends StatefulWidget {
  @override
  _WisataKotaState createState() => _WisataKotaState();
}

class _WisataKotaState extends State<WisataKota> {
  final List<ExperienceCardModel> cardContent = [
    ExperienceCardModel(1,"Jakarta", "Taman wisata BKT","Rp. 20.000/org",false),
    ExperienceCardModel(1,"Bandung", "Taman wisata tangkuban perahu","Rp. 100.000/org",false),
    ExperienceCardModel(1,"Jakarta", "Taman wisata TMII","Rp. 10.000/org",false),
    ExperienceCardModel(1,"Jogja", "Taman wisata Prambanan","Rp. 120.000/org",false),
    ExperienceCardModel(1,"Bali", "Taman wisata Nusa 2","Rp. 150.000/org",false),
    // CardContent(Icons.article_outlined, "Daftar"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 16),
        color: Colors.white,
        child: GridView.builder(
          shrinkWrap: true,
          // padding: const EdgeInsets.symmetric(horizontal: 30),
          itemCount: cardContent.length,
          itemBuilder: (ctx, index){
            return Container(
              margin: EdgeInsets.only(bottom: 24,right: index % 2 == 0 ?6:24,left: index % 2 != 0 ?6:24),
              child: ExperienceCard(
                cardContent[index].location,
                cardContent[index].description,
                cardContent[index].price,
                LikeButton(
                  size: 25,
                  circleColor:
                  CircleColor(start: Color(0xff00ddff), end: Color(0xff0099cc)),
                  bubblesColor: BubblesColor(
                    dotPrimaryColor: Color(0xff33b5e5),
                    dotSecondaryColor: Color(0xff0099cc),
                  ),
                  likeBuilder: (bool isLiked) {
                    return Icon(
                      isLiked ? Icons.favorite:Icons.favorite_border,
                      color: isLiked ? color_red : Colors.white,
                      size: 25,
                    );
                  },
                ),
                // GestureDetector(
                //   onTap: () {
                //     setState(() {
                //       cardContent[index].isLike = !cardContent[index].isLike;
                //     });
                //     _controller
                //         .reverse()
                //         .then((value) => _controller.forward());
                //   },
                //   child: ScaleTransition(
                //     scale: Tween(begin: 0.7, end: 1.0).animate(
                //         CurvedAnimation(parent: _controller, curve: Curves.easeIn)),
                //     child: cardContent[index].isLike
                //         ? const Icon(
                //       Icons.favorite,
                //       size: 25,
                //       color: Colors.red,
                //     )
                //         : const Icon(
                //       Icons.favorite_border,
                //       color: Colors.white,
                //       size: 25,
                //     ),
                //   ),
                // )
              ),
            );
          },
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 1.0,
            crossAxisSpacing: 0.0,
            mainAxisSpacing: 1,
            mainAxisExtent: 330,
          ),
        ),
      ),
    );
  }
}
