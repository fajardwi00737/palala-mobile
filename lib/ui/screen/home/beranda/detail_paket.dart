import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/widget/card/paket_card.dart';
import 'package:palala_app/ui/widget/card/paket_detail_card.dart';

class DetailPaket extends StatefulWidget {
  @override
  _DetailPaketState createState() => _DetailPaketState();
}

class _DetailPaketState extends State<DetailPaket> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.all(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                onPressed: (){
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back_rounded,size: 25,color: color_secondary.withOpacity(0.87),),
              ),
              SizedBox(height: 45,),
              Text("Pilihan paket",style: Fonts.sh1(),),
              SizedBox(height: 24,),
              Text("Sab, 17 Sep",style: Fonts.sh3(),),
              SizedBox(height: 16,),
              PaketDetailCard(
                "Sab, 17 Sep",
                "2:00 PM - 4:00 PM",
                "Sudah 100 dipesan",
                "Rp200.000 /org",
                onSelected: (){

                },
              ),
              SizedBox(height: 24,),
              PaketDetailCard(
                  "Sab, 17 Sep",
                  "2:00 PM - 4:00 PM",
                  "Sudah 100 dipesan",
                  "Rp200.000 /org",
                onSelected: (){

                },
              ),
              SizedBox(height: 24,),
              Text("Sab, 17 Sep",style: Fonts.sh3(),),
              SizedBox(height: 16,),
              PaketDetailCard(
                  "Sab, 17 Sep",
                  "2:00 PM - 4:00 PM",
                  "Sudah 100 dipesan",
                  "Rp200.000 /org",
                onSelected: (){

                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
