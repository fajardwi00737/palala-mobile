import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/screen/home/beranda/eksibisi_page.dart';
import 'package:palala_app/ui/screen/home/beranda/kuliner_page.dart';
import 'package:palala_app/ui/screen/home/beranda/wisata_alam.dart';
import 'package:palala_app/ui/screen/home/beranda/wisata_kota.dart';
import 'package:palala_app/ui/widget/text_field/search_form_field.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> with SingleTickerProviderStateMixin{
  TabController controller;

  @override

  void initState(){
    controller = new TabController(vsync: this, length: 4);
    //tambahkan SingleTickerProviderStateMikin pada class _HomeState
    super.initState();
  }

  @override
  void dispose(){
    controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: [
          new Scaffold(
            //create appBar
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(145),
              child: new AppBar(
                //warna background
                backgroundColor:Colors.white,
                //judul
                title: Text("tes"),
                //bottom
                bottom: new TabBar(
                  indicatorColor: color_secondary2,
                  unselectedLabelColor: color_secondary2.withOpacity(0.15),
                  controller: controller,
                  tabs: <Widget>[
                    new Tab(icon: new Icon(Icons.place_rounded,color: color_secondary2,),child: Text("Wisata kota",style: Fonts.st3(color: color_secondary2).merge(TextStyle(fontSize: 10)),),),
                    new Tab(icon: new Icon(Icons.airplanemode_active_rounded,color: color_secondary2),child: Text("Eksibisi",style: Fonts.st3(color: color_secondary2).merge(TextStyle(fontSize: 10)),),),
                    new Tab(icon: new Icon(Icons.location_city_outlined,color: color_secondary2),child: Text("Wisata alam",style: Fonts.st3(color: color_secondary2).merge(TextStyle(fontSize: 10)),),),
                    new Tab(icon: new Icon(Icons.local_drink_outlined,color: color_secondary2),child: Text("Kuliner",style: Fonts.st3(color: color_secondary2).merge(TextStyle(fontSize: 10)),),),
                  ],
                ),
              ),
            ),

            //source code lanjutan
            //buat body untuk tab viewnya
            body: Stack(
              children: [

                new TabBarView(
                  //controller untuk tab bar
                  controller: controller,
                  children: <Widget>[
                    //kemudian panggil halaman sesuai tab yang sudah dibuat
                    new WisataKota(),
                    new ExibisiPage(),
                    new WisataAlam(),
                    new KulinerPage()
                  ],
                ),
              ],
            ),
          ),
          new Container(
            margin: EdgeInsets.symmetric(vertical: 16,horizontal: 16),
            child: new SearchFormField(
              "Cari destinasi",
              borderRadius: 28,
            ),
          ),
        ],
      ),
    );
  }
}
