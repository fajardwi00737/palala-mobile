import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/models/menu_list_model/menu_list_model.dart';
import 'package:palala_app/ui/widget/button/custom_border_button.dart';
import 'package:palala_app/ui/widget/card/addons_card.dart';
import 'package:palala_app/ui/widget/card/card_menu_profile.dart';
import 'package:palala_app/ui/widget/card/paket_card.dart';
import 'package:palala_app/ui/widget/card/review_card.dart';

class ProfilPage extends StatefulWidget {
  @override
  _ProfilPageState createState() => _ProfilPageState();
}

class _ProfilPageState extends State<ProfilPage> {

  final List<MenuListModel> cardContent = [
    MenuListModel(1,Icons.person,"Profil"),
    MenuListModel(1,Icons.settings,"Pengaturan"),
    MenuListModel(1,Icons.person_add_alt_1_rounded,"Undang teman"),
    MenuListModel(1,Icons.menu_book_outlined,"Buku banduan"),
    // CardContent(Icons.article_outlined, "Daftar"),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.all(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                  color: color_secondary.withOpacity(0.15),
                  shape: BoxShape.circle,
                ),
              ),
              SizedBox(height: 16,),

              // Container(
              //     height: 48,
              //     width: MediaQuery.of(context).size.width,
              //     child: CustomBorderButton(
              //       title: "Lihat Semua Paket",borderRadius: 6,)),
              SizedBox(height: 22,),

              Text("Fajar",style: Fonts.sh1(),),
              SizedBox(height: 8,),
              Text("Lihat profil",style: Fonts.st2().merge(TextStyle(decoration: TextDecoration.underline,)),),
              SizedBox(height: 39,),
              Text("Pengaturan akun",style: Fonts.sh2(),),
              SizedBox(height: 36,),
              Expanded(
                child: ListView.builder(
                    itemCount: cardContent.length,
                    itemBuilder: (context,index){
                      return CardMenuProfile(cardContent[index].menuName,cardContent[index].icon);
                    }
                ),
              ),

              // PaketCard()
            ],
          ),
        ),
      ),
    );
  }
}
