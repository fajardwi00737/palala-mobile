import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/ui/screen/home/beranda/beranda_page.dart';
import 'package:palala_app/ui/screen/home/beranda/detail_trip_page.dart';
import 'package:palala_app/ui/screen/home/notifikasi/notifikasi.dart';
import 'package:palala_app/ui/screen/home/profil/profil_page.dart';
import 'package:palala_app/ui/screen/home/tersimpan/tersimpan_page.dart';

class HomeNavigation extends StatefulWidget {
  @override
  _HomeNavigationState createState() => _HomeNavigationState();
}

class _HomeNavigationState extends State<HomeNavigation> {
  int _selectedIndex = 0;
  List<Widget> _widgetOptions = <Widget>[];

  _setChildren() async {
    _widgetOptions.add(BerandaPage());
    _widgetOptions.add(TersimpanPage());
    _widgetOptions.add(NotifikasiPage());
    _widgetOptions.add(ProfilPage());
  }

  @override
  void initState() {
    _setChildren();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _buildMain();
  }

  _buildMain() {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Container(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          showUnselectedLabels: false,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              label: 'Beranda',
              icon: Icon(Icons.home_rounded),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite_border_rounded),
              label: 'Tersimpan',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications_outlined),
              label: 'Notifikasi',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profil',
            ),
          ],
          currentIndex: _selectedIndex,
          unselectedItemColor: color_secondary.withOpacity(0.15),
          selectedItemColor: color_primary,
          onTap: _onItemTapped,
        ),
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
