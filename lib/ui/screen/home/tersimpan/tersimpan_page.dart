import 'package:flutter/material.dart';
import 'package:like_button/like_button.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/models/card_model/experience_card_model.dart';
import 'package:palala_app/ui/widget/card/experience_card.dart';
import 'package:palala_app/ui/widget/card/experience_card_list.dart';
import 'package:palala_app/ui/widget/text_field/search_form_field.dart';

class TersimpanPage extends StatefulWidget {
  @override
  _TersimpanPageState createState() => _TersimpanPageState();
}

class _TersimpanPageState extends State<TersimpanPage> {

  final List<ExperienceCardModel> cardContent = [
    ExperienceCardModel(1,"Jakarta", "Taman wisata BKT","Rp. 20.000/org",false,date: "2 - 10 September"),
    ExperienceCardModel(1,"Bandung", "Taman wisata tangkuban perahu","Rp. 100.000/org",false,date: "2 - 10 Juni"),
    ExperienceCardModel(1,"Jakarta", "Taman wisata TMII","Rp. 10.000/org",false,date: "3 - 12 Maret"),
    ExperienceCardModel(1,"Jogja", "Taman wisata Prambanan","Rp. 120.000/org",false,date: "5 - 16 Desember"),
    ExperienceCardModel(1,"Bali", "Taman wisata Nusa 2","Rp. 150.000/org",false,date: "12 - 17 Agustus"),
    // CardContent(Icons.article_outlined, "Daftar"),
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 16,),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  margin: EdgeInsets.only(bottom: 20),
                  child: Text("Tersimpan",style: Fonts.sh1(),)),
              Expanded(
                  child: ListView.builder(
                    itemCount: cardContent.length,
                      itemBuilder: (context, index){
                        return Container(
                          margin: EdgeInsets.symmetric(horizontal: 24,vertical: 12),
                          child: ExperienceCardList(
                            cardContent[index].location,
                            cardContent[index].description,
                            cardContent[index].price,
                            cardContent[index].date,
                            LikeButton(
                              size: 25,
                              circleColor:
                              CircleColor(start: Color(0xff00ddff), end: Color(0xff0099cc)),
                              bubblesColor: BubblesColor(
                                dotPrimaryColor: Color(0xff33b5e5),
                                dotSecondaryColor: Color(0xff0099cc),
                              ),
                              likeBuilder: (bool isLiked) {
                                return Icon(
                                  isLiked ? Icons.favorite:Icons.favorite_border,
                                  color: isLiked ? color_red : Colors.white,
                                  size: 25,
                                );
                              },
                            ),
                            height: 312,
                            width: 312,
                            // GestureDetector(
                            //   onTap: () {
                            //     setState(() {
                            //       cardContent[index].isLike = !cardContent[index].isLike;
                            //     });
                            //     _controller
                            //         .reverse()
                            //         .then((value) => _controller.forward());
                            //   },
                            //   child: ScaleTransition(
                            //     scale: Tween(begin: 0.7, end: 1.0).animate(
                            //         CurvedAnimation(parent: _controller, curve: Curves.easeIn)),
                            //     child: cardContent[index].isLike
                            //         ? const Icon(
                            //       Icons.favorite,
                            //       size: 25,
                            //       color: Colors.red,
                            //     )
                            //         : const Icon(
                            //       Icons.favorite_border,
                            //       color: Colors.white,
                            //       size: 25,
                            //     ),
                            //   ),
                            // )
                          ),
                        );
                      }
                  )
              )
            ],
          ),
        ),
      ),
    );
  }
}
