// import 'package:absen_online/constant/assets_constant.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';

class CustomTextForm extends StatelessWidget {
  CustomTextForm(
      this.lable,this.hintText,{this.onChanged,this.textEditingController,this.type = 1,this.readOnly = false,this.suffixIcon = const Icon(Icons.visibility, size: 16,),this.onTap,this.showPassword,this.focusNode,this.borderRadius = 10.0}
      );
  final int type; // 1. defaul field,2. Email field,  3. password field
  final String lable,hintText;
  final Function onTap;
  final ValueChanged<String> onChanged;
  final TextEditingController textEditingController;
  final Widget suffixIcon;
  final bool showPassword, readOnly;
  final FocusNode focusNode;
  final double borderRadius;
  // final FormFieldSetter<int> onSaved;
  // final FormFieldValidator<String> validator;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: focusNode,
      readOnly: readOnly,
      keyboardType: type == 2 ? TextInputType.emailAddress : TextInputType.name,
      obscureText: type == 3 ? showPassword : false ,
      controller: textEditingController,
      cursorColor: color_secondary,
      onTap: onTap,
      style: Fonts.st1(
        color: color_secondary
      ),
      decoration: InputDecoration(
        suffix: type == 3 ? suffixIcon : null,
        contentPadding: EdgeInsets.all(12),
        labelText: lable,
        floatingLabelBehavior: FloatingLabelBehavior.auto,
        hintText:hintText,
        // hintStyle: TextStyle(
        //   fontFamily: baseUrlFontsPoppinsRegular,
        // ),
        labelStyle: Fonts.st1(
            color: focusNode.hasFocus ? color_secondary : color_secondary.withOpacity(0.12)
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius),
          borderSide: BorderSide(
            color: color_secondary,
            width: 1.5,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius),
          borderSide: BorderSide(
            color: color_secondary.withOpacity(0.15),

          ),
        ),
      ),
      onChanged: onChanged,
      // validator: (value) =>
      // value.isEmpty ? '$lable tidak boleh kosong' : null,
    );
  }
}
