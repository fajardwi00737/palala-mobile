import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';

class SearchFormField extends StatelessWidget {
  SearchFormField(
      this.hintText,{this.onChanged,this.textEditingController,this.type = 1,this.onTap,this.focusNode,this.borderRadius = 10.0}
      );
  final int type; // 1. defaul field,2. Email field,  3. password field
  final String hintText;
  final Function onTap;
  final ValueChanged<String> onChanged;
  final TextEditingController textEditingController;
  final FocusNode focusNode;
  final double borderRadius;
  // final FormFieldSetter<int> onSaved;
  // final FormFieldValidator<String> validator;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(borderRadius),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 0,
            blurRadius: 8,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: TextFormField(
        focusNode: focusNode,
        keyboardType: type == 2 ? TextInputType.emailAddress : TextInputType.name,
        controller: textEditingController,
        cursorColor: color_secondary,
        onTap: onTap,
        style: Fonts.st1(
            color: color_secondary
        ),
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.search_rounded,color: color_secondary.withOpacity(0.15),),
          contentPadding: EdgeInsets.all(12),
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          hintText:hintText,
          // hintStyle: TextStyle(
          //   fontFamily: baseUrlFontsPoppinsRegular,
          // ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(borderRadius),
            borderSide: BorderSide(
              color: color_secondary.withOpacity(0.15),
              width: 0.5,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(borderRadius),
            borderSide: BorderSide(
              color: color_secondary.withOpacity(0.15),
              width: 0.5,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(borderRadius),
            borderSide: BorderSide(
              color: color_secondary.withOpacity(0.15),

            ),
          ),
        ),
        onChanged: onChanged,
        // validator: (value) =>
        // value.isEmpty ? '$lable tidak boleh kosong' : null,
      ),
    );
  }
}
