import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
class CustomBorderButton extends StatelessWidget {
  CustomBorderButton({
    this.onTap,
    this.isEnable : false,
    this.btnLoading : false,
    this.isBorder : false,
    this.title,
    // this.icon,
    this.shadow,
    this.borderRadius,
    this.customText,
    // this.spaceIcon,
    this.color,this.textColor,
    this.hasHeight = false,this.hasWidth = false,

  });
  final bool isEnable, btnLoading, isBorder,hasHeight,hasWidth;
  final Function onTap;
  final String title;
  final Color color,textColor;
  final List<BoxShadow> shadow;
  final Widget  customText;
  final double borderRadius;
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: isEnable ? onTap : null,
      disabledColor: Colors.white,
      disabledTextColor: color_primary,
      textColor: textColor,
      color: Colors.white,
      splashColor: Colors.white.withOpacity(0.5),
      highlightColor: Colors.white.withOpacity(0.5),
      hoverColor: Colors.white.withOpacity(0.5),
      disabledElevation: 0,
      highlightElevation: 0,
      elevation: 0,
      height: 0,
      minWidth: 0,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      padding:EdgeInsets.symmetric(
        vertical:  hasHeight ? 0 :12,
        horizontal:  hasWidth ? 0 : 17,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(borderRadius),
        side: BorderSide(
          color: color_secondary,
          width: 1,
        ) ,
      ),
      child: Row(
        children: [
          // icon != null
          //     ? Flexible(
          //   flex: 1,
          //   child: icon,
          // )
          //     : Container(),
          // icon!= null ? SizedBox(width: spaceIcon,):Container(),
          !btnLoading
              ? customText != null
              ? customText
              : Flexible(
            flex: 4,
            child: Center(
              child: Text(
                  title,
                  style: Fonts.sh3()
              ),
            ),
          )
              : Container(
              height: 20,
              width: 20,
              child: CircularProgressIndicator(backgroundColor: Colors.white)),
        ],
      ),
    );
  }
}
