import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';

class ExperienceOrganizerCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Fajar",style: Fonts.sh3(),),
                  SizedBox(height: 3,),
                  Text("3 Agust 2022",style: Fonts.st2(color: color_secondary.withOpacity(0.38)),),
                ],
              ),
              Container(
                height: 44,
                width: 44,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: color_grey
                ),
              ),
            ],
          ),
          SizedBox(height: 19,),
          Row(
            children: [
              Icon(Icons.star_rounded,size: 25),
              SizedBox(width: 10,),
              Text("300 review",style: Fonts.st2())
            ],
          ),
          SizedBox(height: 10,),
          Row(
            children: [
              Icon(Icons.verified_user_rounded,size: 25,),
              SizedBox(width: 10,),
              Text("Terverifikasi",style: Fonts.st2())
            ],
          )
        ],
      ),
    );
  }
}
