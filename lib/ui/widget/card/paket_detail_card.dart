import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/widget/button/custom_button_primary.dart';

class PaketDetailCard extends StatelessWidget {
  final String date,time,desc,price;
  final Function onTap,onSelected;
  final bool isSelected;
  PaketDetailCard(this.date,this.time,this.desc,this.price,{this.onTap,this.isSelected = false,this.onSelected});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            border: Border.all(
              color: isSelected ? color_primary:color_secondary.withOpacity(0.15),
              width:  isSelected ? 2 : 1,
            )
        ),
        padding: EdgeInsets.all(16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: 4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 3,),
                  Text(time,style: Fonts.st2(),),
                  SizedBox(height: 3,),
                  Text(desc,style: Fonts.st2(),),
                  SizedBox(height: 16,),
                  Text(price,style: Fonts.sh3(color: color_amber)),
                ],
              ),
            ),
            Container(
              width: 84,
              child: CustomButtonPrimary(
                color: color_primary,
                title: "Pilih",
                textColor: Colors.white,
                textSize: 14,
                isEnable: true,
                btnLoading:  false,
                borderRadius: 6,
                onTap: onSelected,
              ),
            )
          ],
        ),
      ),
    );
  }
}
