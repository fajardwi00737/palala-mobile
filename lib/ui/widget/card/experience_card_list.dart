import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';

class ExperienceCardList extends StatelessWidget {
  final String location,description,price,date;
  final Widget likeIcon;
  final double height,width;
  ExperienceCardList(this.location,this.description,this.price,this.date,this.likeIcon,{this.height = 200,this.width = 200});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){

      },
      child: Container(
        decoration: BoxDecoration(
            color:Colors.white,
            borderRadius: BorderRadius.circular(12)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: height,
              width: width,
              decoration: BoxDecoration(
                  color:color_grey,
                  borderRadius: BorderRadius.circular(12)
              ),
              child: Stack(
                children: [
                  Positioned(
                      top: 0,
                      right: 0,
                      child: Container(
                          margin: EdgeInsets.all(8),
                          child: likeIcon))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,

                children: [
                  Text(description,maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.sh3(color: color_secondary),),
                  SizedBox(height: 2,),
                  Text(location,maxLines: 1,style: Fonts.st3(color: color_secondary2.withOpacity(0.5))),
                  SizedBox(height: 2,),
                  Text(date,maxLines: 1,style: Fonts.st3(color: color_secondary2.withOpacity(0.5))),
                  SizedBox(height: 2,),
                  Text(price,maxLines: 1,style: Fonts.sh3(color: color_amber))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

