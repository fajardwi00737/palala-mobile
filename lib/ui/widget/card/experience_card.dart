import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/screen/home/beranda/detail_trip_page.dart';

class ExperienceCard extends StatelessWidget {
  final String location,description,price;
  final Widget likeIcon;
  ExperienceCard(this.location,this.description,this.price,this.likeIcon);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        print("navigate woy");
        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailTripPage()));
      },
      child: Container(
        decoration: BoxDecoration(
            color:Colors.white,
            borderRadius: BorderRadius.circular(12)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 200,
              decoration: BoxDecoration(
                  color:color_grey,
                  borderRadius: BorderRadius.circular(12)
              ),
              child: Stack(
                children: [
                  Positioned(
                      top: 0,
                      right: 0,
                      child: Container(
                          margin: EdgeInsets.all(8),
                          child: likeIcon))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,

                children: [
                  Text(location,maxLines: 1,style: Fonts.st3(color: color_secondary2.withOpacity(0.5))),
                  SizedBox(height: 2,),
                  Text(description,maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.st2(color: color_secondary),),
                  SizedBox(height: 2,),
                  Text(price,maxLines: 1,style: Fonts.sh3(color: color_amber))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

