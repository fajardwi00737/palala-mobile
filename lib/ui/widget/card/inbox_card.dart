import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';

class InboxCard extends StatelessWidget {
  final String name,message, time,date,image;
  InboxCard(this.name,this.message,this.date,this.time,{this.image});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){

      },
      child: Container(
        padding: EdgeInsets.only(top: 12),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 40,
                  width: 40,
                  decoration: BoxDecoration(
                    color: color_secondary.withOpacity(0.15),
                    shape: BoxShape.circle,
                  ),
                ),
                SizedBox(width: 16,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin:EdgeInsets.only(top: 3),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(name,style: Fonts.sh3(),),
                            Text(time,style: Fonts.st3(),)
                          ],
                        ),
                      ),
                      SizedBox(height: 2,),
                      Text(message,overflow: TextOverflow.ellipsis,style: Fonts.st2(color: color_secondary.withOpacity(0.8)),),
                      SizedBox(height: 2,),

                      Text(date,style:Fonts.st2(color: color_secondary.withOpacity(0.8)),),
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 12,),
            Divider(thickness: 1,)
          ],
        ),
      ),
    );
  }
}
