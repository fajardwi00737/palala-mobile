import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';

class CardMenuProfile extends StatelessWidget {
  final String menuName;
  final IconData iconData;
  CardMenuProfile(this.menuName,this.iconData);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){

      },
      child: Container(
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 40,
                  width: 40,

                  child: Icon(iconData),
                ),
                SizedBox(width: 16,),
                Text(menuName)
              ],
            ),
            Divider(thickness: 1,)
          ],
        ),
      ),
    );
  }
}
