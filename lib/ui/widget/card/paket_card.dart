import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/widget/button/custom_button_primary.dart';

class PaketCard extends StatelessWidget {
  final String date,time,desc,price;
  final Function onTap;
  final bool isSelected;
  PaketCard(this.date,this.time,this.desc,this.price,{this.onTap,this.isSelected = false});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: 220,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(
            color: isSelected ? color_primary:color_secondary.withOpacity(0.15),
            width:  isSelected ? 2 : 1,
          )
        ),
        padding: EdgeInsets.all(16),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(date,style: Fonts.sh3(),),
                SizedBox(height: 3,),
                Text(time,style: Fonts.st2(),),
                SizedBox(height: 3,),
                Text(desc,style: Fonts.st2(),),
                SizedBox(height: 16,),
                Text(price,style: Fonts.sh3(color: color_amber)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
