import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';

class ReviewCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      width: 224,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(
            color: color_secondary.withOpacity(0.15),
            width:  1,
          )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: color_grey
                ),
              ),
              SizedBox(width: 12,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Fajar",style: Fonts.sh3(),),
                  SizedBox(height: 3,),
                  Text("3 Agust 2022",style: Fonts.st2(),),
                ],
              )
            ],
          ),
          SizedBox(height: 16,),
          Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu semper eros, quis finibus nisi. Sed in dictum arcu… Baca lebih lanjut",style: Fonts.st2(),)
        ],
      ),
    );
  }
}
