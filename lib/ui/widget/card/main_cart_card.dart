import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';

class MainCartCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            height: 88,
            width: 108,
            decoration: BoxDecoration(
              borderRadius: BorderRadiusDirectional.circular(8),
              color: Colors.grey
            ),
          ),
          SizedBox(
            width: 16,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Banten",style: Fonts.st3(color: Color(0xFF181818).withOpacity(0.5)),),
                SizedBox(
                  height: 4,
                ),
                Text("Wisata Tur Majalengka Jawa Barat",maxLines: 2,overflow: TextOverflow.ellipsis,style: Fonts.sh3(color: color_secondary.withOpacity(0.87)),),
                SizedBox(
                  height: 4,
                ),
                Row(
                  children: [
                    Icon(Icons.star_rounded,size: 22,),
                    Text("4,72",style: Fonts.sh4(color: Color(0xFF181818)),),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
