import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';

class ReviewDetailCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: EdgeInsets.all(16),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: color_grey
                ),
              ),
              SizedBox(width: 12,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Fajar",style: Fonts.sh3(),),
                  SizedBox(height: 3,),
                  Text("3 Agust 2022",style: Fonts.st2(),),
                ],
              )
            ],
          ),
          SizedBox(height: 16,),
          Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu semper eros, quis finibus nisi. Sed in dictum arcu, in molestie ligula. Vivamus sodales sed nulla ut congue.",style: Fonts.st2(),)
        ],
      ),
    );
  }
}
