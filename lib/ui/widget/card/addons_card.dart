import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';

class AddonsCard extends StatelessWidget {
  final Function onTap;
  AddonsCard({this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 210,
      height: 235,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
          border: Border.all(
            color: color_secondary.withOpacity(0.15),
            width:  1,
          )
      ),
      padding: EdgeInsets.symmetric(vertical: 24,horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(Icons.local_pizza_outlined,size: 35,),
          SizedBox(height: 16,),
          Text("Makanan",style: Fonts.sh3()),
          SizedBox(height: 16,),
          Text("Makan Siang dan Cemilan",style: Fonts.st2()),
          Spacer(),
          GestureDetector(
              onTap: onTap,
              child: Text("Lihat semua",style: Fonts.sh(size: 14).merge(TextStyle(decoration: TextDecoration.underline)))),
        ],
      ),
    );
  }
}
