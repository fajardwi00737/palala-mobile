import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';
import 'package:palala_app/ui/widget/button/custom_button_primary.dart';

class ContactHost extends StatefulWidget {
  @override
  _ContactHostState createState() => _ContactHostState();
}

class _ContactHostState extends State<ContactHost> {
  FocusNode myFocusNode = new FocusNode();
  bool enableButton = false, isButtonLoading = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(24),
      child: SingleChildScrollView(
        padding:
        EdgeInsets.only(bottom: 1),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 16,top: 24),
              child: GestureDetector(
                  onTap:(){
                    Navigator.pop(context);
                  },
                  child: Icon(Icons.close_rounded,size: 25,)),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Fajar",style: Fonts.sh3(),),
                    SizedBox(height: 3,),
                    Text("3 Agust 2022",style: Fonts.st2(color: color_secondary.withOpacity(0.38)),),
                  ],
                ),
                Container(
                  height: 44,
                  width: 44,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: color_grey
                  ),
                ),
              ],
            ),
            SizedBox(height: 24,),
            Divider(color: color_secondary.withOpacity(0.12),),
            SizedBox(height: 24,),
            Text("Tinggalkan pesan untuk Fajar",style: Fonts.st2(color: color_secondary.withOpacity(0.87)),),
            SizedBox(height: 16,),
            TextFormField(
              focusNode: myFocusNode,
              keyboardType:  TextInputType.text,
              maxLines: 7,
              // controller: textEditingController,
              cursorColor: color_secondary,
              style: Fonts.st1(
                  color: color_secondary
              ),
              onChanged: (tes){
                if(tes.isNotEmpty){
                  setState(() {
                    enableButton = true;
                  });
                } else {
                  setState(() {
                    enableButton = false;
                  });
                }
              },
              decoration: InputDecoration(
                // suffix: type == 3 ? suffixIcon : null,
                contentPadding: EdgeInsets.all(12),
                floatingLabelBehavior: FloatingLabelBehavior.auto,
                // hintStyle: TextStyle(
                //   fontFamily: baseUrlFontsPoppinsRegular,
                // ),
                labelStyle: Fonts.st1(
                    color: myFocusNode.hasFocus ? color_secondary : color_secondary.withOpacity(0.12)
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide: BorderSide(
                    color: color_secondary,
                    width: 1.5,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide: BorderSide(
                    color: color_secondary.withOpacity(0.15),

                  ),
                ),
              ),
              // validator: (value) =>
              // value.isEmpty ? '$lable tidak boleh kosong' : null,
            ),
            SizedBox(height: 24,),
            Container(
              height: 48,
              width: MediaQuery.of(context).size.width,
              child: CustomButtonPrimary(
                color: color_primary,
                title: "Kirim",
                textColor: Colors.white,
                isEnable: enableButton,
                btnLoading:  isButtonLoading,
                onTap: (){

                  print("sending message");
                },
                borderRadius: 6,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
