import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palala_app/constant/color_constant.dart';
import 'package:palala_app/constant/custom_font.dart';

class AddonsBottomsheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topRight: Radius.circular(16),topLeft:  Radius.circular(16))
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: EdgeInsets.only(left: 30,bottom: 16,top: 24),
            child: Row(
              children: [
                GestureDetector(
                    onTap:(){
                  Navigator.pop(context);
                  },
                    child: Icon(Icons.close_rounded,size: 20,)),
                SizedBox(width: 21,),
                Text("Sudah Termasuk",style: Fonts.sh3(),)
              ],
            ),
          ),
          Divider(color: color_secondary.withOpacity(0.12),),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(24),
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Sudah Termasuk",style: Fonts.sh1(),),
                        SizedBox(height: 24,),
                        Text("Makanan",style: Fonts.sh2(),),
                        SizedBox(height: 16),
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.",style: Fonts.st1(color: color_secondary.withOpacity(0.80)),),
                        SizedBox(height: 52,),
                        Text("Minuman",style: Fonts.sh2(),),
                        SizedBox(height: 16),
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.",style: Fonts.st1(color: color_secondary.withOpacity(0.80)),),
                        SizedBox(height: 52,),
                        Text("Transportasi",style: Fonts.sh2(),),
                        SizedBox(height: 16),
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.",style: Fonts.st1(color: color_secondary.withOpacity(0.80)),),
                        SizedBox(height: 52,),
                        Text("Tiket",style: Fonts.sh2(),),
                        SizedBox(height: 16),
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.",style: Fonts.st1(color: color_secondary.withOpacity(0.80)),),
                        SizedBox(height: 52,),
                        Text("Peralatan",style: Fonts.sh2(),),
                        SizedBox(height: 16),
                        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit.",style: Fonts.st1(color: color_secondary.withOpacity(0.80)),),
                        SizedBox(height: 24),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
