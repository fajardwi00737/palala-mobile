import 'package:flutter/cupertino.dart';

class MenuListModel {
  final int id;
  final IconData icon;
  final String menuName;
  MenuListModel(this.id,this.icon,this.menuName);
}