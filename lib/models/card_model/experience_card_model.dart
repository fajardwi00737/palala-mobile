class ExperienceCardModel {
  final int id;
  final String location;
  final String description;
  final String price;
  bool isLike;
  String date;
  ExperienceCardModel(this.id,this.location,this.description,this.price,this.isLike,{this.date});
}