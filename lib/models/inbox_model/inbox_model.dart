class InboxCardModel {
  final String image;
  final String name;
  final String message;
  final String time;
  final String date;
  InboxCardModel(this.name,this.message,this.time,this.date,{this.image});
}