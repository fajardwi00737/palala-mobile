import 'dart:ui';

const Color color_primary = Color(0xFF00B46E);
const Color color_secondary = Color(0xFF000000);
const Color color_secondary2 = Color(0xFF181818);
const Color color_red = Color(0xFFEA4335);
const Color color_green = Color(0xFF34A853);
const Color color_blue = Color(0xFF4285F4);
const Color color_yellow = Color(0xFFFBBC05);
const Color color_amber = Color(0xFFFA6400);
const Color color_grey = Color(0xFF919EAB);