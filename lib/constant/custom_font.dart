import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:palala_app/constant/color_constant.dart';

TextStyle _heading = GoogleFonts.openSans(
  color: color_secondary,
  fontWeight: FontWeight.bold
);

TextStyle _subHeading = GoogleFonts.openSans(
    color: color_secondary,
  fontWeight: FontWeight.w600,
);

TextStyle _subTitle = GoogleFonts.openSans(
  color: color_secondary,
  fontWeight: FontWeight.normal,
);

TextStyle _body = GoogleFonts.openSans(
  color: color_secondary,
  fontWeight: FontWeight.normal,
);

class Fonts {
  Fonts();
  //
  static TextStyle h({String fontFamily, Color color, double size = 20}) =>
      _heading.merge(
          TextStyle(fontSize: size, fontFamily: fontFamily, color: color));

  static TextStyle h1({String fontFamily, Color color}) =>
      _heading.merge(TextStyle(
        fontSize: 24,
        fontFamily: fontFamily,
        color: color,
      ));

  static TextStyle h2({String fontFamily, Color color}) =>
      _heading.merge(TextStyle(
        fontSize: 20,
        fontFamily: fontFamily,
        color: color,
      ));

  static TextStyle h3({String fontFamily, Color color}) =>
      _heading.merge(TextStyle(
        fontSize: 16,
        fontFamily: fontFamily,
        color: color,
      ));

  static TextStyle h4({String fontFamily, Color color}) =>
      _heading.merge(TextStyle(
        fontSize: 12,
        fontFamily: fontFamily,
        color: color,
      ));

  static TextStyle h5({String fontFamily, Color color}) =>
      _heading.merge(TextStyle(
        fontSize: 10,
        fontFamily: fontFamily,
        color: color,
      ));

  static TextStyle h6({String fontFamily, Color color}) =>
      _heading.merge(TextStyle(
        fontSize: 8,
        fontFamily: fontFamily,
        color: color,
      ));

  //sub heading
  static TextStyle sh({String fontFamily, Color color, double size = 20}) =>
      _subHeading.merge(
          TextStyle(fontSize: size, fontFamily: fontFamily, color: color));

  static TextStyle sh1({String fontFamily, Color color}) =>
      _subHeading.merge(TextStyle(
        fontSize: 24,
        fontFamily: fontFamily,
        color: color,
      ));

  static TextStyle sh2({String fontFamily, Color color}) =>
      _subHeading.merge(TextStyle(
        fontSize: 20,
        fontFamily: fontFamily,
        color: color,
      ));

  static TextStyle sh3({String fontFamily, Color color}) =>
      _subHeading.merge(TextStyle(
        fontSize: 16,
        fontFamily: fontFamily,
        color: color,
      ));

  static TextStyle sh4({String fontFamily, Color color}) =>
      _subHeading.merge(TextStyle(
        fontSize: 12,
        fontFamily: fontFamily,
        color: color,
      ));

  static TextStyle sh5({String fontFamily, Color color}) =>
      _subHeading.merge(TextStyle(
        fontSize: 10,
        fontFamily: fontFamily,
        color: color,
      ));

  static TextStyle sh6({String fontFamily, Color color}) =>
      _subHeading.merge(TextStyle(
        fontSize: 8,
        fontFamily: fontFamily,
        color: color,
      ));

  // Sub title
  static TextStyle st({String fontFamily, Color color, double size = 14, fontWeight = FontWeight.normal}) =>
      _subTitle.merge(TextStyle(
          fontSize: size,
          fontFamily: fontFamily,
          color: color,
          fontWeight: fontWeight));

  static TextStyle st1({String fontFamily, Color color}) =>
      _subTitle.merge(TextStyle(
          fontSize: 16,
          fontFamily: fontFamily,
          color: color,));

  static TextStyle st2({String fontFamily, Color color}) =>
      _subTitle.merge(TextStyle(
          fontSize: 14,
          fontFamily: fontFamily,
          color: color,));

  static TextStyle st3({String fontFamily, Color color}) =>
      _subTitle.merge(TextStyle(
          fontSize: 12,
          fontFamily: fontFamily,
          color: color,));

  static TextStyle st4({String fontFamily, Color color}) =>
      _subTitle.merge(TextStyle(
          fontSize: 10,
          fontFamily: fontFamily,
          color: color,));

  static TextStyle st5({String fontFamily, Color color}) =>
      _subTitle.merge(TextStyle(
          fontSize: 8,
          fontFamily: fontFamily,
          color: color,));

  // Body
  static TextStyle body(
      {String fontFamily,
        Color color,
        double size = 16,
        TextDecoration textDecoration}) =>
      _body.merge(TextStyle(
          fontSize: size,
          fontFamily: fontFamily,
          color: color,
          decoration: textDecoration));

  static TextStyle body2(
      {String fontFamily,
        Color color,
        double size = 14,
        TextDecoration textDecoration}) =>
      _body.merge(TextStyle(
          fontSize: size,
          fontFamily: fontFamily,
          color: color,
          decoration: textDecoration));

  // static TextStyle bodyDark(
  //     {String fontFamily, Color color, double size = 12}) =>
  //     _body.merge(TextStyle(
  //       fontSize: size,
  //       fontFamily: fontFamily,
  //       color: color ?? color_secondary,
  //     ));
}
